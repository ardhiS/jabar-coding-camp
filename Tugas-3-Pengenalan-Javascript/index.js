// SOAL 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// JAWABAN SOAL SATU
let pharasaSatu = pertama.substring(0, 4) + " " + pertama.substring(12, 18) + " " + kedua.substring(0, 7) + " ";
let phrasaDua = kedua.substring(8, 18).toUpperCase();
let tiga = pharasaSatu.concat(phrasaDua);
console.log(tiga);

// SOAL 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// JAWABAN SOAL 2
let hasil = parseInt(kataKetiga) * parseInt(kataKedua) + parseInt(kataPertama) + parseInt(kataKeempat);
console.log(hasil);

// SOAL 3
var kalimat = "wah javascript itu keren sekali";

// JAWABAN SOAL 3
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substr(4, 10);
var kataKetiga = kalimat.substr(15, 3);
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substr(25, 6);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
