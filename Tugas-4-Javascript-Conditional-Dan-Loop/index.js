// Soal 1
/*buatlah variabel seperti di bawah ini

var nilai;

pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

nilai >= 85 indeksnya A
nilai >= 75 dan nilai < 85 indeksnya B
nilai >= 65 dan nilai < 75 indeksnya c
nilai >= 55 dan nilai < 65 indeksnya D
nilai < 55 indeksnya E
*/

// Jawaban Soal 1
var nilai = 85;
if (nilai >= 85) {
  console.log("Index A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Index B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Index C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Index D");
} else {
  console.log("Index E");
}

// Soal 2
/* 
buatlah variabel seperti di bawah ini

var tanggal = 22;
var bulan = 7;
var tahun = 2020;
ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing) 
*/

//jawaban Soal 2
var tanggal = 26;
var bulan = 9;
var tahun = 1995;
var date = new Date(tahun, bulan - 1, tanggal);
switch (bulan) {
  case 9:
    console.log(tanggal + " " + date.toLocaleString("default", { month: "long" }) + " " + tahun);
    break;
  default:
    console.log("Tanggal lahir putin \n");
    break;
}

//Soal 3
/*
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

Output untuk n=3 :
#
##
###
 
Output untuk n=7 :
#
##
###
####
#####
######
#######
*/

//Jawaban Soal 3
// var soal = 3;
var soal = 3;
var hasil = "";
for (var i = 1; i <= soal; i++) {
  for (var j = 1; j <= i; j++) {
    hasil += "# ";
  }
  hasil += "\n";
}
console.log(hasil);

// Soal 4
/*berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
contoh :

Output untuk m = 3

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===

Output untuk m = 5

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript

Output untuk m = 7

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming


Output untuk m = 10

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming
8 - I love Javascript
9 - I love VueJS
=========
10 - I love programming
*/

//Jawaban Soal 4
// var m = 3;
var m = 5;
// var m = 10;
for (var i = 1; i <= m; i++) {
  var kalimat = " - I Love ";

  if (i === 1 || i === 4 || i === 7 || i === 10) {
    var prog = "Programming";
    console.log(i + kalimat + prog);
  } else if (i === 2 || i === 5 || i === 8) {
    var jst = "Javascript";
    console.log(i + kalimat + jst);
  } else if (i === 3 || i === 6 || i === 9) {
    var vjs = "VueJs";
    console.log(i + kalimat + vjs);
  }

  if (i % 3 === 0) {
    var samaDengan = "";
    for (z = 1; z <= i; z++) {
      samaDengan += "=";
    }
    console.log(samaDengan);
  }
}
