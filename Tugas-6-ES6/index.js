// Sola no 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

// Jawaban soal no 1
const rumusPersegiPanjang = (p, l) => {
  const luasPersegiPanjang = (p, l) => {
    return p * l;
  };
  console.log(`Luas Persegi panjangnya adalah : ${luasPersegiPanjang(p, l)}`);

  const kelilingPersegiPanjang = (p, l) => {
    return 2 * (p + l);
  };
  console.log(`Keliling Persegi panjangnya adalah : ${kelilingPersegiPanjang(p, l)}`);

  return;
};
rumusPersegiPanjang(6, 6);
console.log("==========");

// Soal no 2
/*
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
//Driver Code 
newFunction("William", "Imoh").fullName()
*/
// Jawaban soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      return `${firstName} ${lastName}`;
    },
  };
};
console.log(newFunction("William", "Imoh").fullName());
console.log("==========");
// Soal 3
/*
Diberikan sebuah objek sebagai berikut:

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const address = newObject.address;
const hobby = newObject.hobby;
Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// Driver code
console.log(firstName, lastName, address, hobby)
*/

// Jawban soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);
console.log("==========");

// soal no 4
/*
Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/
// Jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined);
console.log("==========");

// Soal no 5
/*
sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
*/

// Jawaban soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);
