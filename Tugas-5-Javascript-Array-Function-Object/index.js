// Soal 1
/*
buatlah variabel seperti di bawah ini

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 
urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

1. Tokek
2. Komodo
3. Cicak
4. Ular
5. Buaya
*/

//Jawaban soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
//menggunakan method short yang mengurutkan nilai masing-masing array berdasarkan angka yang terkecil ke yang terbesar 1-4
var daftarHewanHasilSort = daftarHewan.sort().join("\n");
console.log(daftarHewanHasilSort);

// Soal 2
/*Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]" !
/* 
    Tulis kode function di sini
*/
/*
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
*/

//Jawaban soal 2

function introduce(informasi) {
  //memecah array yang dikirimkan(informasi) kedalam masing-masing variable yang mewakili isi dari variable itu
  var name = informasi.name;
  var age = informasi.age;
  var adress = informasi.address;
  var hobby = informasi.hobby;
  return "Nama saya " + name + ", umur saya " + age + " tahun" + ", alamat saya  di " + adress + ", dan saya punya hobi yaitu " + hobby + ".";
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Soal 3
/*Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2
*/

//Jawaban Soal 3
function hitung_huruf_vokal(klmt) {
  var vokal = 0;

  //melakukan perulangan pada kalimat yang di input dalam fungsi
  for (var i = 0; i <= klmt.length - 1; i++) {
    //kalo ada salah satu huruf vokal, maka kata tersebut dimasukan kedalam variabel vokal
    if (klmt.charAt(i) == "a" || klmt.charAt(i) == "e" || klmt.charAt(i) == "i" || klmt.charAt(i) == "o" || klmt.charAt(i) == "u") {
      //menghitung jumlah huruf vokal yang masuk dalam variabel vokal
      vokal += 1;
    }
  }
  return vokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

//soal 4
/*
Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
*/

//jawaban soal 4

//melakukan operasi hitung pertambahan dan pengurangan dala fungsi hitung
function hitung(a) {
  return a + a - 2;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
