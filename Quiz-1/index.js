// Jawaban soal 2
function jumlah_kata(w) {
  var count = 0;
  var words = w.split(" ");
  for (i = 0; i < words.length; i++) {
    // menghitung kata yang ada dalam variable words. kata tersebut dihitung 1 untuk 1 kata
    if (words[i] != "") {
      count += 1;
    }
  }

  return count;
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

console.log(jumlah_kata(kalimat_1)); // 6
console.log(jumlah_kata(kalimat_2)); // 2
console.log(jumlah_kata(kalimat_3)); // 4
